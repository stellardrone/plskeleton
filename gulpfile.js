'use strict';

var gulp = require('gulp'),
    fs = require('fs-extra'),
    imagemin = require('gulp-imagemin'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    ncp = require('ncp').ncp,
    htmlmin = require('gulp-htmlmin'),
    sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    browserSync = require('browser-sync'),
    cleanCSS = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    newer = require('gulp-newer'),
    create = browserSync.create(),
    reload = browserSync.reload,
    path_to_landings = null,
    domain_name = null,
    bs_config = {
        server: {
            baseDir: "./build"
        },
        tunnel: false,
        host: 'localhost',
        port: 9005,
        logPrefix: "frontend",
        devBaseUrl: 'http://localhost',
    };

function createTree(ar, ix) {
  if (!ix) {
    ix = 0;
  }
  fs.mkdirs(ar[ix], function (err) {
    if (err)
      return console.error(err);
    console.log("Directory structure "+ar[ix]+" created successfully");
    if (ar[ix + 1]) {
      createTree(ar, ix + 1);
    } else {
     	fs.copy('./init/index.html', './src/index.html', {clobber: false}, function (err) {
			  /*if (err) {
			    // i.e. file already exists or can't write to directory 
			    throw err;
			  }*/
			  if (!err)
			  	console.log('./src/index.html file created successfully');
			  fs.copy('./init/style.scss', './src/style/style.scss', {clobber: false}, function (err) {						 
			  	if (!err)
				  	console.log('./src/style/style.scss file created successfully');
				});
			});
    }
  });
}

gulp.task('pl:init', function () {
	var ar = [
		'./src/style',
		'./src/img',
		'./src/js',
		'./src/font',
		'./build/style',
		'./build/img',
		'./build/js',
		'./build/font',
	];
	createTree(ar);
});

gulp.task('pl:export', function () {
  ncp('./build/', path_to_landings+domain_name, function (err) {
    if (err) {
      return console.error(err);
    }
    console.log('done!');
  });
});

gulp.task('pl:zip', function () {
  gulp.src(['./build/*', '!./build/archive.zip'])
  .pipe(zip('archive.zip'))
  .pipe(gulp.dest('./build'));
});

gulp.task('webserver', function () {
	browserSync(bs_config);
});

gulp.task('sass', function () {
	return gulp.src('./src/style/**/*.scss')
	.pipe(autoprefixer({
    browsers: ['last 40 versions'],
    // cascade: false
  }))
  .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
	.pipe(sourcemaps.init())
  .pipe(gulp.dest('./build/style'))
	.pipe(rename({
    // dirname: "",
	  // basename: "main",
    // prefix: "",
    suffix: ".min",
    // extname: ".css"
  }))
  .pipe(cleanCSS({debug: true}, function (details) {
    console.log(details.name + ': ' + details.stats.originalSize);
    console.log(details.name + ': ' + details.stats.minifiedSize);
	}))
  .pipe(gulp.dest('./build/style'))
  .pipe(sourcemaps.write('.', {
		addComment: true,
	}))
  .pipe(gulp.dest('./build/style'))
  .pipe(reload({stream: true}));
});

gulp.task('watch', function () {
	gulp.watch("./src/*.html", ['html']);
  gulp.watch("./src/style/**/*.scss", ['sass']);
	gulp.watch("./src/img/**/*", ['img']);
});

gulp.task('html', function () {
	gulp.src("./src/*.html")
	.pipe(gulp.dest('./build'))
	.pipe(browserSync.stream());
});

gulp.task('img', function () {
  gulp.src('./src/img/**/*')
  .pipe(newer('./build/img'))
  .pipe(imagemin())
  .pipe(gulp.dest('./build/img'))
  .pipe(reload({stream: true}));
});

gulp.task('default', ['sass', 'img', 'html', 'webserver', 'watch']);